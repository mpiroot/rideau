#include "bcstm.h"

#include "file_utils.h"
#include "mem.h"

namespace rideau {

// http://problemkaputt.de/gbatek-3ds-files-sound-wave-streams-cstm-format.htm
// https://github.com/Thealexbarney/DspTool/blob/master/dsptool/decode.c

s16 clampS16(s32 x) {
  if (x > 0x7fff)
    return 0x7fff;
  else if (x < -0x7fff)
    return -0x7fff;
  else
    return (s16)x;
}

void decodeBlock(ReadStream &in, u32 blockSize, s16 *out, u32 &writeIdx,
                 s32 &hist1, s32 &hist2, s16 *coeffs) {

  for (u32 i = 0; i < blockSize; ++i) {
    u8 predScale = in.readu8();
    u8 predictor = (predScale >> 4) & 0xf;
    s32 scale = 1 << (predScale & 0xf);
    s32 coeff1 = coeffs[predictor * 2];
    s32 coeff2 = coeffs[predictor * 2 + 1];

    u8 byte;

    for (u32 s = 0; s < 14; ++s) {
      s32 sample;
      if ((s % 2) == 0) {
        byte = in.readu8();
        sample = (byte >> 4) & 0xf;
      } else
        sample = byte & 0xf;

      if (sample >= 8)
        sample -= 16;

      sample = (((scale * sample) * 2048) + 1024 +
                (coeff1 * hist1 + coeff2 * hist2)) /
               2048;
      sample = clampS16(sample);

      hist2 = hist1;
      hist1 = sample;

      out[writeIdx++] = (s16)sample;
    }
  }
}

void readBcstmFromBuffer(Bcstm *bcstm, const u8 *buffer, usize bufferSize) {
  u8 magic[4];

  ReadStream in = newReadStream(buffer);

  magic[0] = in.readu8();
  magic[1] = in.readu8();
  magic[2] = in.readu8();
  magic[3] = in.readu8();

  ENSURE(magic[0] == 'C' && magic[1] == 'S' && magic[2] == 'T' &&
         magic[3] == 'M');

  u16 bom = in.readu16le();
  ENSURE(bom == 0xfeff);

  u16 headerSize = in.readu16le();
  ENSURE(headerSize == 0x40);

  u32 version = in.readu32le();
  ENSURE(version == 0x02020000 || version == 0x02000000);

  u32 fileSize = in.readu32le();
  ENSURE(fileSize == bufferSize);

  u32 blockCount = in.readu16le();
  ENSURE(blockCount == 3);

  in.m_position += 2; // reserved

  u32 infoBlockRefId = in.readu32le();
  u32 infoBlockOffset = in.readu32le();
  u32 infoBlockSize = in.readu32le();

  u32 seekBlockRefId = in.readu32le();
  u32 seekBlockOffset = in.readu32le();
  u32 seekBlockSize = in.readu32le();

  u32 dataBlockRefId = in.readu32le();
  u32 dataBlockOffset = in.readu32le();
  u32 dataBlockSize = in.readu32le();

  ENSURE(infoBlockRefId == 0x4000);
  ENSURE(seekBlockRefId == 0x4001);
  ENSURE(dataBlockRefId == 0x4002);

  in.m_position += 8; // padding

  ENSURE(in.m_position == headerSize);

  // Info block

  ENSURE(in.m_position == infoBlockOffset);

  u8 chunkId[4];
  chunkId[0] = in.readu8();
  chunkId[1] = in.readu8();
  chunkId[2] = in.readu8();
  chunkId[3] = in.readu8();

  ENSURE(chunkId[0] == 'I' && chunkId[1] == 'N' && chunkId[2] == 'F' &&
         chunkId[3] == 'O');

  u32 infoChunkSize = in.readu32le();

  u32 streamInfoRefId = in.readu32le();
  u32 streamInfoOffset = in.readu32le();

  u32 trackInfoRefId = in.readu32le();
  u32 trackInfoOffset = in.readu32le();

  u32 channelInfoRefId = in.readu32le();
  u32 channelInfoOffset = in.readu32le();

  UNUSED(infoBlockSize);
  UNUSED(infoChunkSize);

  // Stream info entry

  ENSURE(streamInfoRefId == 0x4100);
  static const u32 invalidOffset = 0xffffffff;
  ENSURE(streamInfoOffset != invalidOffset);

  in.m_position = infoBlockOffset + 8 + streamInfoOffset;

  u32 encoding = in.readu8();
  u32 loop = in.readu8();
  u32 channelCount = in.readu8();
  in.m_position += 1; // padding
  u32 sampleRate = in.readu32le();
  u32 loopStartFrame = in.readu32le();
  u32 loopEndFrame = in.readu32le();
  u32 sampleBlockCount = in.readu32le();
  u32 sampleBlockSize = in.readu32le();
  u32 sampleBlockSampleCount = in.readu32le();
  u32 lastSampleBlockSize = in.readu32le();
  u32 lastSampleBlockSampleCount = in.readu32le();
  u32 lastSampleBlockPaddedSize = in.readu32le();
  u32 seekDataSize = in.readu32le();
  u32 seekIntervalSampleCount = in.readu32le();
  u32 sampleDataRefId = in.readu32le();
  u32 sampleDataOffset = in.readu32le();

  ENSURE(encoding == 2); // DSP-ADPCM
  ENSURE(loop == 0);
  ENSURE(loopStartFrame == 0);
  ENSURE(channelCount == 2);
  ENSURE(sampleRate == 32000);

  u32 frameCount = loopEndFrame;

  ENSURE(sampleDataRefId == 0x1f00);

  // Track info entry (ignored)
  UNUSED(trackInfoRefId);
  UNUSED(trackInfoOffset);

  // Channel info entry (ADPCM coefficients)
  UNUSED(channelInfoRefId);
  ENSURE(channelInfoOffset != invalidOffset);

  in.m_position = infoBlockOffset + 8 + channelInfoOffset;

  u32 channelEntriesCount = in.readu32le();
  ENSURE(channelEntriesCount == 2);

  u32 sampleInfoOffset[2];

  for (u32 i = 0; i < 2; ++i) {
    u32 sampleInfoRefId = in.readu32le();
    sampleInfoOffset[i] = in.readu32le();

    ENSURE(sampleInfoRefId == 0x4102);
    ENSURE(sampleInfoOffset[i] != invalidOffset);
  }

  s16 adpcmCoeffs[2][16];
  u8 initialPredScale[2];
  s16 historySample1[2];
  s16 historySample2[2];

  for (u32 c = 0; c < 2; ++c) {
    in.m_position =
        infoBlockOffset + 8 + channelInfoOffset + sampleInfoOffset[c];

    u32 adpcmInfoRefId = in.readu32le();
    u32 adpcmInfoOffset = in.readu32le();

    ENSURE(adpcmInfoRefId == 0x0300);
    ENSURE(adpcmInfoOffset != invalidOffset);

    in.m_position = infoBlockOffset + 8 + channelInfoOffset +
                    sampleInfoOffset[c] + adpcmInfoOffset;

    for (u32 k = 0; k < 16; ++k)
      adpcmCoeffs[c][k] = (s16)in.readu16le();

    initialPredScale[c] = in.readu8();
    in.m_position += 1; // padding
    historySample1[c] = (s16)in.readu16le();
    historySample2[c] = (s16)in.readu16le();
  }

  // Seek block (unused since we convert to PCM16)
  UNUSED(seekBlockOffset);
  UNUSED(seekBlockSize);
  UNUSED(seekDataSize);
  UNUSED(seekIntervalSampleCount);

  // Data block (ADPCM samples)

  ENSURE(dataBlockOffset != invalidOffset);
  in.m_position = dataBlockOffset;

  chunkId[0] = in.readu8();
  chunkId[1] = in.readu8();
  chunkId[2] = in.readu8();
  chunkId[3] = in.readu8();

  ENSURE(chunkId[0] == 'D' && chunkId[1] == 'A' && chunkId[2] == 'T' &&
         chunkId[3] == 'A');

  u32 dataChunkSize = in.readu32le();
  ENSURE(dataChunkSize == dataBlockSize);

  bcstm->numChannels = 2;
  bcstm->frameCount = frameCount;
  bcstm->sampleRate = sampleRate;
  bcstm->samplesPCM[0] = (s16 *)allocMem(frameCount * sizeof(s16));
  bcstm->samplesPCM[1] = (s16 *)allocMem(frameCount * sizeof(s16));

  in.m_position = dataBlockOffset + 8 + sampleDataOffset;
  // Sample data is interleaved per block (channel 0 block 0, channel 1 block 0,
  // channel 0 block 1, channel 1 block, etc.).

  s32 hist1[2];
  s32 hist2[2];
  u32 writeIdx[2];

  for (u32 c = 0; c < channelCount; ++c) {
    hist1[c] = historySample1[c];
    hist2[c] = historySample2[c];
    writeIdx[c] = 0;
  }

  for (u32 i = 0; i < sampleBlockCount; ++i) {

    // FIXME: still missing a few samples from the last block
    u32 blockSize =
        (i < (sampleBlockCount - 1)) ? sampleBlockSize : lastSampleBlockSize;

    blockSize >>= 3;

    for (u32 c = 0; c < channelCount; ++c) {
      decodeBlock(in, blockSize, bcstm->samplesPCM[c], writeIdx[c], hist1[c],
                  hist2[c], adpcmCoeffs[c]);
    }
  }

  UNUSED(initialPredScale);

  // ASSERT(sampleBlockSampleCount == frameCount) ?

  // printf("version: %x\n", version);
  // printf("sampleRate: %u\n", sampleRate);
  // printf("loopStart: %u\n", loopStartFrame);
  // printf("loopEnd: %u\n", loopEndFrame);
  // printf("sampleBlockCount: %u\n", sampleBlockCount);
  // printf("sampleBlockSize: %u\n", sampleBlockSize);
  // printf("sampleBlockSampleCount: %u\n", sampleBlockSampleCount);
}

void readBcstmFromFile(Bcstm *bcstm, const char *filename) {
  ENSURE(filename != nullptr);
  ENSURE(bcstm != nullptr);

  usize fileSize = getFileSize(filename);
  u8 *raw = (u8 *)allocMem(fileSize);

  readFile(filename, raw, fileSize);
  readBcstmFromBuffer(bcstm, raw, fileSize);

  freeMem(raw);
}

void Bcstm::deinit() {
  for (u32 c = 0; c < numChannels; ++c)
    freeMem(samplesPCM[c]);
}

} // namespace rideau
