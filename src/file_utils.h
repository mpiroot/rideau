#ifndef FILE_UTILS_H
#define FILE_UTILS_H

#include <stdio.h>

#include "utils.h"

namespace rideau {

struct ReadStream {
  const u8 *m_buffer;
  usize m_position;

  u32 readu32le();
  u16 readu16le();
  u8 readu8();
};

ReadStream newReadStream(const u8 *buf);

struct WriteStream {
  u8 *m_buffer;
  usize m_position;

  void writeu32le(u32 x);
  void writeu16le(u16 x);
  void writeu8(u8 x);
};

WriteStream newWriteStream(u8 *buf);

u32 readu32le(const u8 *p);
u16 readu16le(const u8 *p);
u8 readu8(const u8 *p);

void writeu32le(u8 *p, u32 x);
void writeu16le(u8 *p, u16 x);
void writeu8(u8 *p, u8 x);

usize getFileSize(const char *filename);
void readFile(const char *filename, u8 *buffer, usize bufferSize);
void writeFile(const char *filename, const u8 *buffer, usize bufferSize);

} // namespace rideau

#endif
