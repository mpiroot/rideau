#include "mem.h"

#include <stdlib.h>

namespace rideau {

static usize allocCounter = 0;
static bool initialized = false;

void initMem() {
  ENSURE(!initialized);
  allocCounter = 0;
  initialized = true;
}

void deinitMem() {
  ENSURE(initialized);
  ENSURE(allocCounter == 0);

  initialized = false;
}

void *allocMem(usize byteSize) {
  ASSERT(initialized);
  ENSURE(byteSize > 0);

  void *p = malloc(byteSize);
  ENSURE(p != nullptr);

  allocCounter++;

  return p;
}

void freeMem(void *p) {
  ASSERT(initialized);
  ENSURE(p != nullptr);
  free(p);

  ASSERT(allocCounter > 0);
  allocCounter--;
}

} // namespace rideau
