#ifndef OPTIONS_H
#define OPTIONS_H

namespace rideau {

struct Options {
  bool batchMode;
  const char *triggerFile;
  const char *musicFile;
};

int parseArgs(int argc, char *argv[], Options *options);

} // namespace rideau

#endif
