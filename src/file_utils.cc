#include "file_utils.h"

namespace rideau {

u32 readu32le(const u8 *p) {
  ENSURE(p != nullptr);

#if IS_LITTLE_ENDIAN
  u32 x = *(u32 *)p;
#elif IS_BIG_ENDIAN
  u32 x = (u32(p[3]) << 24) | (u32(p[2]) << 16) | (u32(p[1]) << 8) | u32(p[0]);
#endif

  return x;
}

void writeu32le(u8 *p, u32 x) {
  ENSURE(p != nullptr);

#if IS_LITTLE_ENDIAN
  *(u32 *)p = x;
#elif IS_BIG_ENDIAN
  p[0] = x & 0xFF;
  p[1] = (x >> 8) & 0xFF;
  p[2] = (x >> 16) & 0xFF;
  p[3] = (x >> 24) & 0xFF;
#endif
}

u16 readu16le(const u8 *p) {
  ENSURE(p != nullptr);

#if IS_LITTLE_ENDIAN
  u16 x = *(u16 *)p;
#elif IS_BIG_ENDIAN
  u16 x = (u16)((u16(p[1]) << 8) | u16(p[0]));
#endif

  return x;
}

void writeu16le(u8 *p, u16 x) {
  ENSURE(p != nullptr);

#if IS_LITTLE_ENDIAN
  *(u16 *)p = x;
#elif IS_BIG_ENDIAN
  p[0] = x & 0xFF;
  p[1] = (x >> 8) & 0xFF;
#endif
}

void writeu8(u8 *p, u8 x) {
  ENSURE(p != nullptr);

  *p = x;
}

u8 readu8(const u8 *p) {
  ENSURE(p != nullptr);

  u8 x = *p;

  return x;
}

usize getFileSize(const char *filename) {
  ENSURE(filename != nullptr);

  FILE *f = fopen(filename, "rb");
  ENSURE(f != nullptr);

  int ret = fseek(f, 0, SEEK_END);
  ENSURE(ret == 0);
  long fileSize = ftell(f);
  ENSURE(fileSize > 0);

  ret = fclose(f);
  ENSURE(ret == 0);

  return (usize)fileSize;
}

void readFile(const char *filename, u8 *buffer, usize bufferSize) {
  ENSURE(filename != nullptr);

  FILE *f = fopen(filename, "rb");
  ENSURE(f != nullptr);

  usize readSize = fread(buffer, sizeof(u8), bufferSize, f);
  ENSURE(readSize == bufferSize);

  int ret = fclose(f);
  ENSURE(ret == 0);
}

void writeFile(const char *filename, const u8 *buffer, usize bufferSize) {
  ENSURE(filename != nullptr);

  FILE *f = fopen(filename, "wb");
  ENSURE(f != nullptr);

  usize writeSize = fwrite(buffer, sizeof(u8), bufferSize, f);
  ENSURE(writeSize == bufferSize);

  int ret = fclose(f);
  ENSURE(ret == 0);
}

ReadStream newReadStream(const u8 *buf) {
  ReadStream s;
  s.m_buffer = buf;
  s.m_position = 0;
  return s;
}

u32 ReadStream::readu32le() {
  u32 x = rideau::readu32le(m_buffer + m_position);
  m_position += 4;
  return x;
};

u16 ReadStream::readu16le() {
  u16 x = rideau::readu16le(m_buffer + m_position);
  m_position += 2;
  return x;
};

u8 ReadStream::readu8() {
  u8 x = rideau::readu8(m_buffer + m_position);
  m_position += 1;
  return x;
};

WriteStream newWriteStream(u8 *buf) {
  WriteStream s;
  s.m_buffer = buf;
  s.m_position = 0;
  return s;
}

void WriteStream::writeu32le(u32 x) {
  rideau::writeu32le(m_buffer + m_position, x);
  m_position += 4;
};

void WriteStream::writeu16le(u16 x) {
  rideau::writeu16le(m_buffer + m_position, x);
  m_position += 2;
};

void WriteStream::writeu8(u8 x) {
  rideau::writeu8(m_buffer + m_position, x);
  m_position += 1;
};

} // namespace rideau
