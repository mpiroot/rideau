#include "lz11.h"

#include "file_utils.h"

namespace rideau {

u32 getLZ11UncompressedSize(const u8 *buf) {
  const u32 header = readu32le(buf);
  const u32 type = header & 0xFF;
  const u32 size = header >> 8;

  ENSURE(type == 0x11); // LZ11

  return size;
}

void decompressLZ11(const u8 *src, usize srcSize, u8 *dst, usize dstSize) {
  ENSURE(src != nullptr);
  ENSURE(dst != nullptr);

  u32 uncompressedSize = getLZ11UncompressedSize(src);
  ENSURE(dstSize >= uncompressedSize);

  usize readIdx = 4; // past header
  usize writeIdx = 0;

  while (writeIdx < uncompressedSize) {
    u32 flags = src[readIdx++];

    for (u8 bit = 0; bit < 8; ++bit) {
      if (writeIdx >= uncompressedSize)
        break;

      if ((flags & 0x80) == 0) {
        dst[writeIdx++] = src[readIdx++];
      } else {
        const u32 x = src[readIdx++];
        const u32 hi = x >> 4;
        const u32 lo = x & 0xF;

        u32 len;
        u32 disp;

        if (hi == 1) {
          const u32 b0 = src[readIdx++];
          const u32 b1 = src[readIdx++];
          const u32 b2 = src[readIdx++];

          len = ((lo << 12) | (b0 << 4) | (b1 >> 4)) + 0x111;
          disp = (((b1 & 0xF) << 8) | b2) + 1;
        } else if (hi == 0) {
          const u32 b0 = src[readIdx++];
          const u32 b1 = src[readIdx++];

          len = ((lo << 4) | (b0 >> 4)) + 0x11;
          disp = (((b0 & 0xF) << 8) | b1) + 1;
        } else {
          const u8 b0 = src[readIdx++];

          len = hi + 1;
          disp = ((lo << 8) | b0) + 1;
        }

        ASSERT((len & 0xffff) == len);

        for (u32 i = 0; i < len; ++i) {
          dst[writeIdx] = dst[writeIdx - disp];
          writeIdx++;
        }
      }

      flags <<= 1;
    }
  }

  ENSURE(readIdx <= srcSize);
  ENSURE(writeIdx <= dstSize);
}

usize compressLZ11(const u8 *src, usize srcSize, u8 *dst, usize dstSize) {
  ENSURE(src != nullptr);
  ENSURE(dst != nullptr);

  usize readIdx = 0;
  usize writeIdx = 0;

  const u32 lz11Header = NumCast<u32>(srcSize << 8) | 0x11;
  writeu32le(dst, lz11Header);
  writeIdx += 4;

  // TODO: actual compression
  while (readIdx < srcSize) {
    u32 flags = 0x00;
    dst[writeIdx++] = (u8)flags;

    for (u32 bit = 0; bit < 8; ++bit) {
      if (readIdx < srcSize) {
        dst[writeIdx++] = src[readIdx++];
      } else {
        dst[writeIdx++] = 0;
      }
    }
  }

  ENSURE(readIdx <= srcSize);
  ENSURE(writeIdx <= dstSize);

  return writeIdx;
}

} // namespace rideau
