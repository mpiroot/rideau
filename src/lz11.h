#ifndef LZ11_H
#define LZ11_H

#include "utils.h"

namespace rideau {

u32 getLZ11UncompressedSize(const u8 *buf);
void decompressLZ11(const u8 *src, usize srcSize, u8 *dst, usize dstSize);
usize compressLZ11(const u8 *src, usize srcSize, u8 *dst, usize dstSize);

} // namespace rideau

#endif
