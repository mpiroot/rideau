#ifndef MEM_H
#define MEM_H

#include "utils.h"

namespace rideau {

void initMem();
void deinitMem();
void *allocMem(usize byteSize);
void freeMem(void *p);

} // namespace rideau

#endif
