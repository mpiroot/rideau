#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

typedef size_t usize;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define IS_LITTLE_ENDIAN 1
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define IS_BIG_ENDIAN 1
#else
#error "Unknown endianness"
#endif

#define UNUSED(V) ((void)(V))
#define STATIC_ASSERT(x) static_assert(x, "")
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

#define UNREACHABLE() __builtin_unreachable()

#define ENSURE(x)                                                              \
  if (!(x)) {                                                                  \
    fprintf(stderr, "%s:%u: Assert failed '%s'\n", __FILE__, __LINE__, #x);    \
    *(volatile int *)0 = 0;                                                    \
  }

#define ASSERT(x) ENSURE(x)

template <typename T, typename F> T NumCast(F n) {
  T cast = static_cast<T>(n);
  ASSERT(static_cast<F>(cast) == n);
  return cast;
}

#endif
