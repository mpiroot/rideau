#define IMGUI_DEFINE_MATH_OPERATORS

#include "imgui.h"
#include "imgui_impl_sdl.h"
#include "imgui_impl_opengl2.h"
#include "imgui_internal.h"

#include "SDL.h"
#include "GL/gl.h"

#include "bcstm.h"

#include <atomic>
#include <chrono>
#include <vector>

#include "track.h"
#include "options.h"
#include "file_utils.h"
#include "mem.h"
#include "version.h"

// TODO: time loading bcstm/resampling?
// TODO: add frametime/fps when debugging

// TODO: isolate std stuff

// TODO: take folders as arguments, edit all tracks within, let other tool unpack/pack the rom as needed
// TODO: beat bars

// TODO: copy & paste?
// TODO: drag&drop?

// TODO: EMS auto angles
// TODO: EMS feature zone
// TODO: EMS curves (still messy)
// TODO: FMS auto curves

namespace rideau {

static u32 g_idCounter = 1; // 0 is not assigned
u32 genId() { return g_idCounter++; }

void resampleCubic(const s16 *samples, usize sampleCount,
                   float *resampledBuffer, usize targetBufferCount,
                   u32 inputFreq, u32 outputFreq) {
  const double ratio = (double)inputFreq / outputFreq;
  ASSERT(ratio > 0);
  double mu = 0.0;
  double s[4] = {0, 0, 0, 0};

  float *pOut = resampledBuffer;
  usize writtenCount = 0;

  for (usize i = 0; i < sampleCount; ++i) {
    double sample = ImClamp((float)samples[i] / 32768.0f, -1.0f, 1.0f);

    s[0] = s[1];
    s[1] = s[2];
    s[2] = s[3];
    s[3] = sample;

    while (mu <= 1.0) {
      double A = s[3] - s[2] - s[0] + s[1];
      double B = s[0] - s[1] - A;
      double C = s[2] - s[0];
      double D = s[1];

      *pOut++ = float(ImClamp(A * mu * mu * mu + B * mu * mu + C * mu + D, -1.0, 1.0));
      writtenCount++;

      if (writtenCount == targetBufferCount)
        return;

      mu += ratio;
    }

    mu -= 1.0;
  }

  ENSURE((usize)(pOut - resampledBuffer) <= targetBufferCount);
}

struct Editor {
  std::atomic<bool> isAudioPlaying;
  std::atomic<usize> currentFrame;
  std::atomic<float> audioVolume;

  std::vector<u32> selectedTriggers;
  bool isSeeking;
  bool shouldSortTriggers;
  bool trackModified;

  double estimatedCurrentFrame;

  float scaleX;

  u32 sampleRate;
  float *samples;
  usize framesCount;

  GLuint waveformTexture;

  void init(Bcstm *bcstm) {
    selectedTriggers.clear();
    isSeeking = false;
    shouldSortTriggers = false;
    isAudioPlaying = false;
    audioVolume = 0.5f;
    currentFrame = 0;
    estimatedCurrentFrame = 0;
    trackModified = false;

    scaleX = 4.0f;

    // Resample to 48000Hz float for greater backend compatibility (JACK at
    // least doesn't want anything else)
    sampleRate = 48000;
    framesCount = bcstm->frameCount * (u64)sampleRate / bcstm->sampleRate;
    ENSURE(bcstm->numChannels == 2);
    float *samplesLeft = (float *)malloc(framesCount * sizeof(float));
    float *samplesRight = (float *)malloc(framesCount * sizeof(float));
    ENSURE(samplesLeft != nullptr);
    ENSURE(samplesRight != nullptr);
    resampleCubic(bcstm->samplesPCM[0], bcstm->frameCount,
                  samplesLeft, framesCount,
                  (u32)bcstm->sampleRate, sampleRate);
    resampleCubic(bcstm->samplesPCM[1], bcstm->frameCount,
                  samplesRight, framesCount,
                  (u32)bcstm->sampleRate, sampleRate);

    samples = (float*)malloc(framesCount * 2 * sizeof(float));
    ENSURE(samples != nullptr);

    for (usize i=0; i < framesCount; ++i) {
      samples[i * 2] = samplesLeft[i];
      samples[i * 2 + 1] = samplesRight[i];
    }

    free(samplesLeft);
    free(samplesRight);
  }

  bool isTriggerSelected(u32 triggerId) const {
    // return std::find(selectedTriggers.begin(), selectedTriggers.end(),
    //                  triggerId) != selectedTriggers.end();
    return false;
  }

  void selectTrigger(u32 triggerId, bool append) {
    // if (!append)
    //   selectedTriggers.clear();
    // selectedTriggers.push_back(triggerId);
  }

  void unselectTrigger(u32 triggerId) {
    // auto it =
    //     std::find(selectedTriggers.begin(), selectedTriggers.end(), triggerId);
    // if (it != selectedTriggers.end())
    //   selectedTriggers.erase(it);
  }

  void unselectAllTriggers() { selectedTriggers.clear(); }

  void initWaveformTexture() {
    const u32 texWidth = 32;
    const u32 texHeight = 8192;
    u8 *texData = (u8 *)malloc(texWidth * texHeight * 3);
    ENSURE(texData != nullptr);
    u8 *pData = texData;

    {
      ASSERT(texWidth < framesCount);
      const double texelPerFrame = (double)texHeight / NumCast<double>(framesCount);
      double t = 0;
      usize frameIdx = 0;

      const u8 bgColor[3] = {44, 51, 56};
      const u8 waveformColor[3] = {77, 124, 160};

      for (u32 y = 0; y < texHeight; ++y) {
        float minSample = +1.0f;
        float maxSample = -1.0f;
        while (t < 1.0) {
          if (frameIdx < framesCount) {
            const float left = samples[frameIdx * 2];
            const float right = samples[frameIdx * 2 + 1];
            frameIdx++;
            const float sample = ImClamp((left + right) / 2.0f, -1.0f, 1.0f);
            minSample = std::min(minSample, sample);
            maxSample = std::max(maxSample, sample);
          } else {
            minSample = maxSample = 0;
            break;
          }
          t += texelPerFrame;
        }
        t -= 1.0;

        u32 lineStart = (u32)((minSample + 1.0f) / 2.0f * texWidth);
        u32 lineEnd = (u32)((maxSample + 1.0f) / 2.0f * texWidth);

        u32 x = 0;
        while (x < lineStart) {
          *pData++ = bgColor[0];
          *pData++ = bgColor[1];
          *pData++ = bgColor[2];
          ++x;
        }
        while (x < lineEnd) {
          *pData++ = waveformColor[0];
          *pData++ = waveformColor[1];
          *pData++ = waveformColor[2];
          ++x;
        }
        while (x < texWidth) {
          *pData++ = bgColor[0];
          *pData++ = bgColor[1];
          *pData++ = bgColor[2];
          ++x;
        }
      }
    }

    glGenTextures(1, &waveformTexture);
    glBindTexture(GL_TEXTURE_2D, waveformTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texWidth, texHeight, 0, GL_RGB,
                 GL_UNSIGNED_BYTE, texData);

    free(texData);
  }

  void deinit() {
    free(samples);
    samples = nullptr;

    glDeleteTextures(1, &waveformTexture);
  }

  void seekTo(usize frame) {
    currentFrame = frame;
    estimatedCurrentFrame = NumCast<double>(frame);
  }

  void togglePause() {
    isAudioPlaying = !isAudioPlaying;
    if (isAudioPlaying && currentFrame >= framesCount)
      seekTo(0);
  }
};

void audioCallback(void * userData, u8* outBuffer, int outBufferSize) {

  Editor *editor = (Editor*)userData;
  const bool isPlaying = editor->isAudioPlaying;

  memset(outBuffer, 0, (usize)outBufferSize);

  if (!isPlaying) {
    return;
  }

  const usize framesCount = editor->framesCount;
  const usize currentFrame = editor->currentFrame;
  const usize remainingFrames = framesCount - currentFrame;
  const usize requestedFrames = (usize)outBufferSize / 8;

  usize framesToCopy;
  if (requestedFrames > remainingFrames) {
    framesToCopy = remainingFrames;
    editor->isAudioPlaying = false;
    editor->currentFrame = framesCount;
  }
  else {
    framesToCopy = requestedFrames;
    editor->currentFrame += framesToCopy;
  }

  const usize currentSample = currentFrame * 2;
  const usize samplesToCopy= framesToCopy * 2;

  float *inputSamples = &editor->samples[currentSample];
  float *outSamples = (float*)outBuffer;
  const float volume = editor->audioVolume;

  for (usize i=0; i < samplesToCopy; ++i) {
    outSamples[i] = inputSamples[i] * volume;
  }
}

struct AudioStuff {
  SDL_AudioDeviceID deviceId;
  u32 bufferSize;
};

int initAudio(int sampleRate, Editor *editor, AudioStuff &audio) {
  ENSURE(editor != nullptr);

  SDL_AudioSpec want, have;
  SDL_AudioDeviceID device;

  memset(&want, 0, sizeof(want));
  want.freq = sampleRate;
  want.format = AUDIO_F32;
  want.channels = 2;
  want.samples = 512;
  want.callback = audioCallback;
  want.userdata = editor;
  device = SDL_OpenAudioDevice(NULL, 0, &want, &have, 0);

  if (device == 0){
    fprintf(stderr, "Couldn't initialize SDL device: %s\n", SDL_GetError());
    return 1;
  }

  ASSERT(have.samples == want.samples);

  audio.deviceId = device;
  audio.bufferSize = want.samples;

  SDL_PauseAudioDevice(device, 0);

  return 0;
}

void deinitAudio(AudioStuff &audio) {
  SDL_CloseAudioDevice(audio.deviceId);
}

void drawTrack(Track &track, Editor &editor) {

  const float triggerRadius = 10.0f;
  const float scaleX = editor.scaleX;
  const ImColor touchTriggerColor(0.8f, 0.2f, 0.2f);
  const ImColor touchTriggerColorTransparent(0.8f, 0.2f, 0.2f, 0.5f);
  const ImColor slideTriggerColor(0.8f, 0.8f, 0.2f);
  const ImColor slideTriggerArrowColor(1.0f, 1.0f, 1.0f);
  const ImColor holdTriggerColor(0.2f, 0.8f, 0.2f);
  const ImColor holdLineColor(0.15f, 0.5f, 0.15f);
  const ImColor featureZoneColor(0.2f, 0.3f, 0.7f, 0.2f);
  const ImColor summonColor(0.7f, 0.6f, 0.2f, 0.2f);
  const ImColor trackGuideColor(0.9f, 0.9f, 0.9f);
  const ImColor currentlyPlayingColor(1.0f, 1.0f, 1.0f);
  const ImColor unknownColor(0.8f, 0.2f, 0.8f);

  // Audio scrub zone
  {
    const int windowHeight = 32;

    ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.17f, 0.20f, 0.22f, 1.0f));
    ImGui::BeginChild("Audio Scrub", ImVec2(0, windowHeight), false);
    ImGui::PopStyleColor();

    const ImColor cursorColor = ImColor(1.0f, 1.0f, 1.0f);

    ImVec2 orig = ImGui::GetCurrentWindow()->DC.CursorPos;
    ImDrawList *drawList = ImGui::GetWindowDrawList();

    const float windowWidth = ImGui::GetWindowWidth();

    // Waveform (from a texture, 'cause it's expensive to draw at runtime)
    drawList->AddImageQuad((ImTextureID)(intptr_t)editor.waveformTexture,
                           orig + ImVec2(windowWidth, windowHeight),
                           orig + ImVec2(windowWidth, 0), orig,
                           orig + ImVec2(0, windowHeight), ImVec2(1, 0),
                           ImVec2(0, 0), ImVec2(0, 1), ImVec2(1, 1));

    // Feature zones
    const float pixelsPerTick = (float)windowWidth / NumCast<float>(track.tickCount);
    drawList->AddRectFilled(
        orig + ImVec2(windowWidth - (float)track.featureZoneStart * pixelsPerTick, 0),
        orig + ImVec2(windowWidth - (float)track.featureZoneEnd * pixelsPerTick,
                      windowHeight),
        featureZoneColor);

    // Summon
    drawList->AddRectFilled(
        orig + ImVec2(windowWidth - (float)track.summonStart * pixelsPerTick, 0),
        orig +
        ImVec2(windowWidth - (float)track.summonEnd * pixelsPerTick, windowHeight),
        summonColor);

    // Cursor
    {
      const float scrubPos =
        (1.0f - ((float)editor.estimatedCurrentFrame / NumCast<float>(editor.framesCount))) *
          windowWidth;
      const float x = roundf(scrubPos);

      drawList->AddLine(orig + ImVec2(x, 0), orig + ImVec2(x, windowHeight),
                        cursorColor);
    }

    // Click to seek
    if (ImGui::IsWindowHovered() && ImGui::IsMouseClicked(0)) {
      ImVec2 mouseRelPos = ImGui::GetMousePos() - orig;
      const usize seekFrame =
        (usize)((1.0f - (mouseRelPos.x / windowWidth)) * NumCast<float>(editor.framesCount));
      editor.seekTo(seekFrame);
      editor.isSeeking = true;
    }

    ImGui::EndChild();
  }

  const float ticksPerFrame = NumCast<float>(track.tickCount) / NumCast<float>(editor.framesCount);
  const float currentTick = float(editor.estimatedCurrentFrame * ticksPerFrame);

  if (track.isEMS()) {
    static float scale = 0.5f;
    ImGui::SliderFloat("Scale", &scale, -2.0f, 2.0f);

    static bool hideTriggers = false;
    ImGui::Checkbox("Hide triggers", &hideTriggers);

    ImGui::PushStyleVar(ImGuiStyleVar_ScrollbarSize, 30.0f);
    ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.17f, 0.20f, 0.22f, 1.0f));
    const int lastTriggerTick = track.triggers[track.triggerCount - 1].tick;
    const int contentPad = 2000;
    const int contentWidth = lastTriggerTick + contentPad;
    ImGui::SetNextWindowContentSize(ImVec2((float)contentWidth, 0));

    ImGui::BeginChild("Track", ImVec2(400, 200), false,
                      ImGuiWindowFlags_HorizontalScrollbar);

    ImGui::PopStyleColor();
    ImGui::PopStyleVar();

    ImDrawList *drawList = ImGui::GetWindowDrawList();

    const ImGuiWindow *window = ImGui::GetCurrentWindow();
    ImVec2 orig = window->DC.CursorPos;

    const float tickWindow = 200.0f;
    float currentScrollTick = ImGui::GetScrollX();
    const u32 tickStart =
      (u32)roundf(ImClamp(currentScrollTick - tickWindow, 0.0f, (float)lastTriggerTick));
    const u32 tickEnd =
      (u32)roundf(ImClamp(currentScrollTick + tickWindow, 0.0f, (float)lastTriggerTick));

    orig.x += currentScrollTick;

    ImVec2 prevHoldTriggerPos;
    bool hasPrevHoldTrigger = false;

    for (u32 i = 0; i < track.triggerCount; ++i) {
      const Trigger &t = track.triggers[i];

      if (t.tick < tickStart || t.tick > tickEnd)
        continue;

      ImColor col;
      if (t.type == Trigger::Touch)
        col = touchTriggerColor;
      else if (t.type == Trigger::Slide)
        col = slideTriggerColor;
      else if (t.type == Trigger::Hold || t.type == Trigger::Holdlet ||
               t.type == Trigger::HoldEnd || t.type == Trigger::HoldEndSlide)
        col = holdTriggerColor;
      else if (t.type == Trigger::TrackGuide)
        col = trackGuideColor;
      else
        col = unknownColor;

      float triggerX;
      float triggerY;
      if (t.type == Trigger::TrackGuide) {
        triggerX = NumCast<float>(t.x);
        triggerY = NumCast<float>(t.y);
      } else {
        Trigger *prevTrackGuide = nullptr;
        for (int j = i - 1; j >= 0; --j) {
          if (track.triggers[j].type == Trigger::TrackGuide) {
            prevTrackGuide = &track.triggers[j];
            break;
          }
        }
        ENSURE(prevTrackGuide != nullptr);

        Trigger *nextTrackGuide = nullptr;
        for (u32 j = i + 1; j < track.triggerCount; ++j) {
          if (track.triggers[j].type == Trigger::TrackGuide) {
            nextTrackGuide = &track.triggers[j];
            break;
          }
        }
        ENSURE(nextTrackGuide != nullptr);

        const float r = float(t.tick - prevTrackGuide->tick) /
                        float(nextTrackGuide->tick - prevTrackGuide->tick);
        triggerX = ImLerp((float)prevTrackGuide->x, (float)nextTrackGuide->x, r);
        triggerY = ImLerp((float)prevTrackGuide->y, (float)nextTrackGuide->y, r);
      }

      const float posy = orig.y + 100.0f - triggerY;
      const float posx = orig.x + 200.0f + triggerX;

      // Draw track guide
      if (t.type == Trigger::TrackGuide) {
        Trigger *prevTrackGuide = nullptr;
        for (int j = i - 1; j >= 0; --j) {
          if (track.triggers[j].type == Trigger::TrackGuide) {
            prevTrackGuide = &track.triggers[j];
            break;
          }
        }
        if (prevTrackGuide != nullptr) {
          const ImVec2 prev = ImVec2(orig.x + 200.0f + (float)prevTrackGuide->x,
                                     orig.y + 100.0f - (float)prevTrackGuide->y);
          const ImVec2 pos = ImVec2(posx, posy);

          if (prevTrackGuide->flags & Trigger::Flag::CurveInward ||
              prevTrackGuide->flags & Trigger::Flag::CurveOutward) {
            const ImVec2 v0 = prev;
            const ImVec2 v1 = pos;
            const ImVec2 normal =
                ImRotate((v1 - v0), cosf(IM_PI / 2), sinf(IM_PI / 2));
            const float reverse =
                prevTrackGuide->flags & Trigger::Flag::CurveInward ? -1.0f
                                                                   : 1.0f;
            const ImVec2 cp = v0 + (v1 - v0) / 2 + (normal * scale * reverse);
            drawList->AddBezierQuadratic(v0, cp, v1, trackGuideColor, 1.0f, 0);
          } else {
            drawList->AddLine(prev, pos, trackGuideColor, 1.0f);
          }
        }
      }

      // Draw line from previous hold trigger
      if (hasPrevHoldTrigger && !hideTriggers &&
          (t.type == Trigger::HoldEnd || t.type == Trigger::HoldEndSlide ||
           t.type == Trigger::Holdlet)) {
        drawList->AddLine(prevHoldTriggerPos, ImVec2(posx, posy), holdLineColor,
                          10.0f);
      }
      if (t.type == Trigger::Hold || t.type == Trigger::HoldEnd ||
          t.type == Trigger::HoldEndSlide || t.type == Trigger::Holdlet) {
        prevHoldTriggerPos = ImVec2(posx, posy);
        hasPrevHoldTrigger = true;
      }

      // Draw trigger circle
      float radius = triggerRadius;
      if (t.type == Trigger::Holdlet)
        radius /= 2.0f;
      else if (t.type == Trigger::TrackGuide)
        radius = 3.0f;

      if (t.type == Trigger::TrackGuide) {
        drawList->AddCircleFilled(ImVec2(posx, posy), radius, col);
      } else {
        if (hideTriggers)
          continue;

        if (t.type == Trigger::Holdlet || t.type == Trigger::HoldEnd ||
            t.type == Trigger::HoldEndSlide)
          drawList->AddCircleFilled(ImVec2(posx, posy), radius, col);
        else
          drawList->AddCircle(ImVec2(posx, posy), radius, col, 0, 8.0f);
      }

      // Draw slide direction
      // FIXME: in EMS, angle is automatic unless the absolute angle flag is
      // set
      if (t.type == Trigger::Slide || t.type == Trigger::HoldEndSlide) {
        Trigger *nextTrackGuide = nullptr;
        for (u32 j = i + 1; j < track.triggerCount; ++j) {
          if (track.triggers[j].type == Trigger::TrackGuide) {
            nextTrackGuide = &track.triggers[j];
            break;
          }
        }
        ENSURE(nextTrackGuide != nullptr);

        const ImVec2 vec = ImVec2(orig.x + 200.0f + (float)nextTrackGuide->x,
                                  orig.y + 100.0f - (float)nextTrackGuide->y) -
                           ImVec2(posx, posy);

        float a;
        if (t.flags & Trigger::Flag::AbsoluteAngle) {
          a = 0.0f; // TODO:
        } else {
          a = atan2f(vec.y, vec.x);
        }
        const ImVec2 center = ImVec2(posx, posy);
        const ImVec2 dir = ImRotate(ImVec2(12.0f, 0.0f), cosf(a), sinf(a));
        drawList->AddCircleFilled(center + dir, 5.0f,
                                  ImColor(1.0f, 1.0f, 1.0f));
      }

      // Tooltip
      const ImRect bb(posx - radius, posy - radius, posx + radius,
                      posy + radius);

      if (ImGui::IsMouseHoveringRect(bb.Min, bb.Max)) {
        ImGui::BeginTooltip();
        ImGui::Text("%d %s (%d)", i, TRIGGER_TYPE_NAMES[t.type], t.type);
        ImGui::Text("tick: %u", t.tick);
        ImGui::Text("x: %d", t.x);
        ImGui::Text("y: %d", t.y);
        ImGui::Text("angle: %d", t.angle);
        ImGui::Text("flags: %04x", t.flags);
        if (t.flags & Trigger::Flag::AbsoluteAngle)
          ImGui::Text("Absolute angle");
        if (t.flags & Trigger::Flag::CurveInward)
          ImGui::Text("Curve inward");
        if (t.flags & Trigger::Flag::CurveOutward)
          ImGui::Text("Curve outward");
        ImGui::EndTooltip();
      }
    }

    ImGui::EndChild();
  } else {
    const float windowWidth = ImGui::GetWindowContentRegionWidth();
    const float windowHeight = 400.0f;
    const float contentWidth = NumCast<float>(track.tickCount) * scaleX;

    ImGui::PushStyleVar(ImGuiStyleVar_ScrollbarSize, 30.0f);
    ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.17f, 0.20f, 0.22f, 1.0f));
    ImGui::SetNextWindowContentSize(ImVec2(contentWidth, 0));

    static bool scrollFuse = true;
    if (scrollFuse) {
      ImGui::SetNextWindowScroll(ImVec2(contentWidth, 0));
      scrollFuse = false;
    }

    ImGui::BeginChild("Track", ImVec2(windowWidth, windowHeight), false,
                      ImGuiWindowFlags_HorizontalScrollbar);

    ImGui::PopStyleColor();
    ImGui::PopStyleVar();

    if (editor.isAudioPlaying || editor.isSeeking) {
      const float currentTickScreenOffset = windowWidth * 3.0f / 4.0f;
      ImGui::SetScrollX(contentWidth - currentTick * scaleX -
                        currentTickScreenOffset);
    }

    ImDrawList *drawList = ImGui::GetWindowDrawList();

    const ImGuiWindow *window = ImGui::GetCurrentWindow();
    const ImVec2 orig = window->DC.CursorPos;

    ImVec2 prevHoldTriggerPos;
    float prevHoldTriggerTick;

    // Feature zone
    drawList->AddRectFilled(
        orig + ImVec2(contentWidth - (float)track.featureZoneStart * scaleX, 0),
        orig +
        ImVec2(contentWidth - (float)track.featureZoneEnd * scaleX, windowHeight),
        featureZoneColor);

    // Summon
    drawList->AddRectFilled(
        orig + ImVec2(contentWidth - (float)track.summonStart * scaleX, 0),
        orig + ImVec2(contentWidth - (float)track.summonEnd * scaleX, windowHeight),
        summonColor);

    const float laneHeight = track.isBMS() ? 60.0f : 1.0f;
    bool mouseOverTrigger = false;

    const u32 tickAtScrollBegin = NumCast<u32>(floorf(contentWidth - ImGui::GetScrollX() / scaleX));
    const u32 tickAtScrollEnd = NumCast<u32>(ceilf(
        (contentWidth - (ImGui::GetScrollX() + ImGui::GetWindowWidth())) /
        scaleX));
    const u32 slack = 20;
    const u32 cullTickMin =
        tickAtScrollEnd > slack ? tickAtScrollEnd - slack : 0;
    const u32 cullTickMax = tickAtScrollBegin + slack;

    for (u32 i = 0; i < track.triggerCount; ++i) {
      const Trigger &t = track.triggers[i];

      ImColor col;
      if (t.type == Trigger::Touch)
        col = touchTriggerColor;
      else if (t.type == Trigger::Slide)
        col = slideTriggerColor;
      else if (t.type == Trigger::Hold || t.type == Trigger::Holdlet ||
               t.type == Trigger::HoldEnd || t.type == Trigger::HoldEndSlide)
        col = holdTriggerColor;
      else
        col = unknownColor;

      // Highlight
      if (editor.isAudioPlaying && fabs((float)t.tick - currentTick) < 4.0f) {
        col = currentlyPlayingColor;
      }

      const float posy = orig.y + 100 + float(t.y) * laneHeight;
      const float posx = orig.x + contentWidth - (NumCast<float>(t.tick) * scaleX);

      // Draw line from previous hold trigger
      if (t.type == Trigger::HoldEnd || t.type == Trigger::HoldEndSlide ||
          t.type == Trigger::Holdlet) {

        ImColor c = holdLineColor;
        if (editor.isAudioPlaying && currentTick > prevHoldTriggerTick &&
            currentTick < (float)t.tick)
          c = currentlyPlayingColor;

        drawList->AddLine(prevHoldTriggerPos - ImVec2(0.5f, 0.5f),
                          ImVec2(posx, posy) - ImVec2(0.5f, 0.5f), c, 10.0f);
      }
      if (t.type == Trigger::Hold || t.type == Trigger::Holdlet) {
        prevHoldTriggerPos = ImVec2(posx, posy);
        prevHoldTriggerTick = NumCast<float>(t.tick);
      }

      // Cull out of view triggers
      if (t.tick > cullTickMax || t.tick < cullTickMin)
        continue;

      // Draw trigger circle
      float radius = triggerRadius;
      if (t.type == Trigger::Holdlet)
        radius /= 2.0f;

      if (t.type == Trigger::Holdlet || t.type == Trigger::HoldEnd ||
          t.type == Trigger::HoldEndSlide)
        drawList->AddCircleFilled(ImVec2(posx, posy), radius, col);
      else
        drawList->AddCircle(ImVec2(posx, posy), radius, col, 0, 8.0f);

      if (editor.isTriggerSelected(t.id))
        drawList->AddCircle(ImVec2(posx, posy), radius * 2,
                            ImColor(1.0f, 1.0f, 1.0f), 0, 2.0f);

      // Draw slide arrow
      if (t.type == Trigger::Slide || t.type == Trigger::HoldEndSlide) {
        const float a = ((float)t.angle + 180.0f) * 2.0f * IM_PI / 360.0f;
        const ImVec2 center = ImVec2(posx, posy);
        const ImVec2 dir = ImRotate(ImVec2(0, 12.0f), cosf(a), sinf(a));
        const ImVec2 head0 = ImRotate(ImVec2(-6.0f, 12.0f), cosf(a), sinf(a));
        const ImVec2 head1 =
            ImRotate(ImVec2(0.0f, 12.0f + 10.0f), cosf(a), sinf(a));
        const ImVec2 head2 = ImRotate(ImVec2(+6.0f, 12.0f), cosf(a), sinf(a));

        drawList->AddLine(center - ImVec2(0.5f, 0.5f),
                          center + dir - ImVec2(0.5f, 0.5f),
                          slideTriggerArrowColor, 3.0f);
        drawList->AddTriangleFilled(center + head0, center + head1,
                                    center + head2, slideTriggerArrowColor);
      }

      // Tooltip
      const ImRect bb(posx - radius, posy - radius, posx + radius,
                      posy + radius);

      if (ImGui::IsMouseHoveringRect(bb.Min, bb.Max)) {
        ImGui::BeginTooltip();
        ImGui::Text("idx: %d id: %d %s", i, t.id, TRIGGER_TYPE_NAMES[t.type]);
        ImGui::Text("tick: %u", t.tick);
        ImGui::Text("y: %d", t.y);
        ImGui::Text("angle: %d", t.angle);
        ImGui::EndTooltip();

        if (ImGui::IsMouseClicked(0)) {
          bool append = SDL_GetModState() & KMOD_SHIFT;
          editor.selectTrigger(t.id, append);
        }

        mouseOverTrigger = true;

        if (ImGui::IsMouseClicked(1)) {
          // Remove trigger
          // if (editor.isTriggerSelected(t.id))
          //   editor.unselectTrigger(t.id);
          // track.triggers.erase(track.triggers.begin() + i);
          // track.triggerCount--;
          // editor.trackModified = true;
        }
      }
    }

    // Draw new trigger under cursor
    if (ImGui::IsWindowHovered() && !mouseOverTrigger) {
      ImVec2 mouseRelPos = ImGui::GetMousePos() - orig;
      // Snap to nearest lane
      u32 nearestLane = (u32)roundf(((mouseRelPos.y - 100.0f) / laneHeight));
      nearestLane = ImClamp(nearestLane, 0u, 3u);
      u32 newTick = (u32)roundf((contentWidth - mouseRelPos.x) / scaleX);

      ImVec2 overlayPos = ImVec2(contentWidth - (float)newTick * scaleX,
                                 (float)nearestLane * laneHeight + 100.0f);
      drawList->AddCircle(orig + overlayPos, triggerRadius,
                          touchTriggerColorTransparent, 0, 8.0f);

      ImGui::BeginTooltip();
      ImGui::Text("%u", newTick);
      ImGui::EndTooltip();

      // Add trigger
      if (ImGui::IsMouseClicked(0)) {
        // Trigger t;
        // t.type = Trigger::Type::Touch;
        // t.tick = newTick;
        // t.x = 0;
        // t.y = NumCast<s32>(nearestLane);
        // t.angle = 0;
        // t.flags = Trigger::Flag::None;
        // t.id = genId();

        // track.triggers.push_back(t);
        // track.triggerCount++;

        // editor.trackModified = true;
        // editor.shouldSortTriggers = true;
        // editor.selectTrigger(t.id, false);
      }
    }

    // Draw currently playing tick
    {
      const ImColor playingColor = ImColor(1.0f, 1.0f, 1.0f);
      const ImColor pausedColor = ImColor(0.5f, 0.5f, 0.5f);
      const ImColor color = editor.isAudioPlaying ? playingColor : pausedColor;
      const float tickPos = contentWidth - currentTick * scaleX;
      const float x = roundf(tickPos);

      drawList->AddLine(orig + ImVec2(x, 0), orig + ImVec2(x, windowHeight),
                        color);
    }

    ImGui::EndChild();
  }
}

} // namespace rideau

int main(int argc, char *argv[]) {
  using namespace rideau;

  Options options;

  if (parseArgs(argc, argv, &options) != 0) {
    fprintf(stderr, "Usage: %s [-b] TRIGGER_FILE MUSIC_FILE\n", argv[0]);
    return EXIT_FAILURE;
  }

  initMem();

  Bcstm bcstm;
  readBcstmFromFile(&bcstm, options.musicFile);

  Editor editor;
  editor.init(&bcstm);

  Track track;
  readTrackFromFile(&track, options.triggerFile);
  checkTrack(track);

  for (u32 i = 0; i < track.triggerCount; ++i)
    track.triggers[i].id = genId();

  if (options.batchMode) {
    printTrackStats(track);
    return EXIT_SUCCESS;
  }

  // Fix tickCount to 59.825 TPS
  // const u32 newTickCount = u32(
  //     59.825 * ((double)editor.framesCount / editor.sampleRate));
  // if (track.tickCount != newTickCount) {
  //   track.tickCount = newTickCount;
  //   track.tickEnd = track.tickCount;
  //   editor.trackModified = true;
  // }

  // Init video
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
    return 1;

  AudioStuff audioStuff;
  {
    int ret = initAudio(NumCast<int>(editor.sampleRate), &editor, audioStuff);
    ENSURE(ret == 0);
  }

  // GL 2.1
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

  const char windowTitle[128] = {};
  snprintf(const_cast<char *>(windowTitle), ARRAY_SIZE(windowTitle),
           "rideau %d.%d", RIDEAU_VERSION_MAJOR, RIDEAU_VERSION_MINOR);

  const int windowWidth = 800;
  const int windowHeight = 600;

  SDL_WindowFlags windowFlags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL);
  SDL_Window *window = SDL_CreateWindow(windowTitle, SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, windowWidth,
                              windowHeight, windowFlags);
  if (window == nullptr)
    return 1;

  SDL_GLContext glContext = SDL_GL_CreateContext(window);
  if (glContext == nullptr)
    return 1;
  if (SDL_GL_MakeCurrent(window, glContext) != 0)
    return 1;

  SDL_GL_SetSwapInterval(1); // Enable vsync

  ImGui::CreateContext();
  ImVec4 clear_color = ImVec4(0.f, 0.f, 0.f, 0.f);

  // Setup Platform/Renderer bindings
  ImGui_ImplSDL2_InitForOpenGL(window, glContext);
  ImGui_ImplOpenGL2_Init();

  ImGui::GetStyle().Colors[ImGuiCol_WindowBg] =
      ImVec4(0.15f, 0.17f, 0.18f, 1.0f);

  editor.initWaveformTexture();

  auto lastLoopTime = std::chrono::high_resolution_clock::now();

  bool isRunning = true;

  while (isRunning) {
    SDL_Event e;
    while (SDL_PollEvent(&e)) {
      ImGui_ImplSDL2_ProcessEvent(&e);

      switch (e.type) {
      case SDL_QUIT:
        isRunning = false;
        break;

      case SDL_KEYDOWN:
        if (e.key.keysym.scancode == SDL_SCANCODE_ESCAPE) {
          isRunning = false;
        }

        if (e.key.keysym.scancode == SDL_SCANCODE_SPACE) {
          editor.togglePause();
        }

        if (e.key.keysym.scancode == SDL_SCANCODE_S && e.key.keysym.mod & KMOD_CTRL) {
          writeTrackToFile(track, options.triggerFile);
          editor.trackModified = false;
        }

        break;
      }
    }

    if (!isRunning)
      break;

    auto loopTime = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float, std::micro> loopDtUs =
        (loopTime - lastLoopTime);
    lastLoopTime = loopTime;
    u32 loopUs = static_cast<u32>(loopDtUs.count());

    if (editor.isAudioPlaying) {
      const double usToSec = 1e-6;
      editor.estimatedCurrentFrame += loopUs * usToSec * editor.sampleRate;
    }

    if (editor.shouldSortTriggers) {
      // Keep the triggers sorted by increasing tick
      // std::sort(track.triggers.begin(), track.triggers.end(),
      //           [&](Trigger &a, Trigger &b) { return a.tick < b.tick; });
    }
    if (editor.isSeeking)
      editor.isSeeking = false;

    // Draw
    int display_w, display_h;
    SDL_GL_GetDrawableSize(window, (int *)&display_w, (int *)&display_h);

    // Draw ImGui
    ImGui_ImplOpenGL2_NewFrame();
    ImGui_ImplSDL2_NewFrame(window);
    ImGui::NewFrame();

    {
      ImGui::SetNextWindowPos(ImVec2(0, 0));
      ImGui::SetNextWindowSize(ImVec2(NumCast<float>(display_w), NumCast<float>(display_h)));
      ImGui::Begin("Main", nullptr,
                   ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize |
                       ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse |
                       ImGuiWindowFlags_NoBringToFrontOnFocus);

      const ImColor headerColor = ImColor(0.6f, 0.8f, 1.0f);

      {
        ImGui::BeginGroup();
        ImGui::TextColored(headerColor, "Track info");

        ImGui::Text("Type: ");
        ImGui::SameLine();

        ImGui::PushStyleColor(ImGuiCol_Text, (ImVec4)ImColor(0.5f, 1.0f, 0.5f));
        if (ImGui::RadioButton("FMS", track.isFMS())) {
          track.trackType = Track::Type::FMS;
          editor.trackModified = true;
        }
        ImGui::PopStyleColor();
        ImGui::SameLine();
        ImGui::PushStyleColor(ImGuiCol_Text, (ImVec4)ImColor(1.0f, 0.5f, 0.5f));
        if (ImGui::RadioButton("BMS", track.isBMS())) {
          track.trackType = Track::Type::BMS;
          editor.trackModified = true;
        }
        ImGui::SameLine();
        ImGui::PopStyleColor();
        ImGui::PushStyleColor(ImGuiCol_Text, (ImVec4)ImColor(0.3f, 0.7f, 1.0f));
        if (ImGui::RadioButton("EMS", track.isEMS())) {
          track.trackType = Track::Type::EMS;
          editor.trackModified = true;
        }
        ImGui::PopStyleColor();

        ImGui::Text("Triggers: %d", track.triggerCount);

        ImGui::Text("Ticks: ");
        ImGui::SameLine();
        ImGui::Text("%d", track.tickCount);

        const double ticksPerSecond =
          (double)track.tickCount / ((double)editor.framesCount / editor.sampleRate);
        ImGui::SameLine();
        ImGui::Text("TPS: %.3f", ticksPerSecond);

        ImGui::Text("Feature Zone: ");
        ImGui::SameLine();
        ImGui::SetNextItemWidth(100.0f);
        if (ImGui::SliderInt("##featureZoneStart",
                             (int *)&track.featureZoneStart, (int)track.tickStart,
                             (int)track.featureZoneEnd)) {
          editor.trackModified = true;
        }
        ImGui::SameLine();
        ImGui::SetNextItemWidth(100.0f);
        if (ImGui::SliderInt("##featureZoneEnd", (int *)&track.featureZoneEnd,
                             (int)track.featureZoneStart, (int)track.tickEnd)) {
          editor.trackModified = true;
        }

        ImGui::Text("Summon: ");
        ImGui::SameLine();
        ImGui::SetNextItemWidth(100.0f);
        if (ImGui::SliderInt("##summonStart", (int *)&track.summonStart,
                             (int)track.tickStart, (int)track.summonEnd)) {
          editor.trackModified = true;
        }
        ImGui::SameLine();
        ImGui::SetNextItemWidth(100.0f);
        if (ImGui::SliderInt("##summonEnd", (int *)&track.summonEnd,
                             (int)track.summonStart, (int)track.tickEnd)) {
          editor.trackModified = true;
        }

        ImGui::EndGroup();
      }

      ImGui::SameLine();

      // Edit trigger
      {
        #if 0
        ImGui::BeginGroup();
        ImGui::TextColored(headerColor, "Trigger info");

        if (editor.selectedTriggers.empty()) {
          ImGui::Text("Select a trigger to edit it");

          if (ImGui::Button("Select all")) {
            for (u32 i = 0; i < track.triggers.size(); ++i)
              editor.selectTrigger(track.triggers[i].id, true);
          }

        } else if (editor.selectedTriggers.size() > 1) {
          ImGui::Text("%zu triggers selected", editor.selectedTriggers.size());

          std::vector<u32> toErase;
          toErase.reserve(editor.selectedTriggers.size());

          Trigger *rep = nullptr;
          for (u32 i = 0; i < track.triggers.size(); ++i) {
            if (editor.isTriggerSelected(track.triggers[i].id)) {
              rep = &track.triggers[i];
              break;
            }
          }

          if (ImGui::Button("Delete selected")) {
            for (u32 i = 0; i < track.triggers.size();) {
              if (editor.isTriggerSelected(track.triggers[i].id)) {
                track.triggers.erase(track.triggers.begin() + i);
                track.triggerCount--;
              } else {
                ++i;
              }
            }
            editor.trackModified = true;
            editor.unselectAllTriggers();
          }

          ImGui::SetNextItemWidth(100.0f);
          if (ImGui::SliderInt("Lane", &rep->y, 0, 3)) {
            for (u32 i = 0; i < track.triggerCount; ++i) {
              if (editor.isTriggerSelected(track.triggers[i].id)) {
                track.triggers[i].y = rep->y;
              }
            }
            editor.trackModified = true;
          }

        } else {
          ASSERT(editor.selectedTriggers.size() == 1);

          Trigger *t = nullptr;
          u32 selectedTriggerIndex;
          for (u32 i = 0; i < track.triggerCount; ++i) {
            if (editor.isTriggerSelected(track.triggers[i].id)) {
              t = &track.triggers[i];
              selectedTriggerIndex = i;
              break;
            }
          }
          ASSERT(t != nullptr);

          if (ImGui::Button("Delete")) {
            // editor.unselectTrigger(t->id);
            // track.triggers.erase(track.triggers.begin() + selectedTriggerIndex);
            // track.triggerCount--;
            // editor.trackModified = true;
          }

          for (u32 i = 0; i < Trigger::Type::TrackGuide; ++i) {
            if (i > 0)
              ImGui::SameLine();
            if (ImGui::RadioButton(TRIGGER_TYPE_NAMES[i], t->type == i)) {
              t->type = (Trigger::Type)i;
              editor.trackModified = true;
            }
          }

          if (ImGui::SliderInt("Tick", (int *)&t->tick, track.tickStart,
                               track.tickEnd)) {
            editor.shouldSortTriggers = true;
            editor.trackModified = true;
          }
          ImGui::SetNextItemWidth(100.0f);
          if (ImGui::SliderInt("Lane", &t->y, 0, 3)) {
            editor.trackModified = true;
          }
          if (t->type == Trigger::Slide || t->type == Trigger::HoldEndSlide) {
            ImGui::SameLine();
            ImGui::SetNextItemWidth(100.0f);
            if (ImGui::SliderInt("Angle", (int *)&t->angle, 0, 360)) {
              editor.trackModified = true;
            }
          }
        }

        ImGui::EndGroup();
        #endif
      }

      {
        ImGui::BeginGroup();

        const bool colorButton = editor.trackModified;
        if (colorButton) {
          ImGui::PushStyleColor(ImGuiCol_Button,
                                (ImVec4)ImColor::HSV(0, 0.6f, 0.6f));
          ImGui::PushStyleColor(ImGuiCol_ButtonHovered,
                                (ImVec4)ImColor::HSV(0, 0.7f, 0.7f));
          ImGui::PushStyleColor(ImGuiCol_ButtonActive,
                                (ImVec4)ImColor::HSV(0, 0.8f, 0.8f));
        }
        if (ImGui::Button("Save track")) {
          writeTrackToFile(track, options.triggerFile);
          editor.trackModified = false;
        }
        if (colorButton)
          ImGui::PopStyleColor(3);

        ImGui::SameLine();

        const char *playLabel = editor.isAudioPlaying ? "Pause" : "Play";
        if (ImGui::Button(playLabel))
          editor.togglePause();

        ImGui::SameLine();
        ImGui::SetNextItemWidth(150.0f);
        float volumeDb = 10 * log10f(editor.audioVolume);
        if (ImGui::SliderFloat("Volume", &volumeDb, -50.0f, 0.0f)) {
          editor.audioVolume = powf(10.0f, volumeDb / 10.0f);
        }

        ImGui::SameLine();
        ImGui::SetNextItemWidth(100.0f);
        ImGui::SliderFloat("Speed", &editor.scaleX, 2.0f, 20.0f);

        ImGui::EndGroup();
      }

      drawTrack(track, editor);

      ImGui::End();
    }

    ImGui::Render();
    ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());

    // Swap
    SDL_GL_SwapWindow(window);
  }

  ImGui_ImplOpenGL2_Shutdown();
  ImGui_ImplSDL2_Shutdown();
  ImGui::DestroyContext();

  SDL_GL_DeleteContext(glContext);
  SDL_DestroyWindow(window);

  deinitAudio(audioStuff);

  SDL_Quit();

  bcstm.deinit();

  deinitMem();

  return EXIT_SUCCESS;
}
