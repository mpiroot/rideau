#ifndef BRSTM_H
#define BRSTM_H

#include "utils.h"

namespace rideau {

struct Bcstm {
  usize frameCount;
  u32 numChannels;
  u32 sampleRate;
  s16 *samplesPCM[2];

  void deinit();
};

void readBcstmFromBuffer(Bcstm *brstm, const u8 *buffer, usize bufferSize);
void readBcstmFromFile(Bcstm *brstm, const char *filename);

} // namespace rideau

#endif
