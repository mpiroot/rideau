#include "track.h"

#include "file_utils.h"
#include "lz11.h"
#include "mem.h"

namespace rideau {

void readTrackFromBuffer(Track *track, const u8 *raw, u32 rawSize) {
  ENSURE(raw != nullptr);
  ENSURE(track != nullptr);

  ReadStream r = newReadStream(raw);

  track->trackType = Track::Type(r.readu32le());
  ASSERT(track->trackType < Track::Type::Count);
  track->tickCount = r.readu32le();
  track->tickStart = r.readu32le();
  track->tickEnd = r.readu32le();
  track->featureZoneStart = r.readu32le();
  track->featureZoneEnd = r.readu32le();
  track->summonStart = r.readu32le();
  track->summonEnd = r.readu32le();
  track->summonTrigger = r.readu32le();
  track->triggerCount = r.readu32le();

  for (u32 i = 0; i < track->triggerCount; ++i) {
    Trigger &t = track->triggers[i];

    t.tick = r.readu32le();
    t.type = Trigger::Type(r.readu32le());
    ASSERT(t.type < Trigger::Type::Count);
    t.x = (s32)r.readu32le();
    t.y = (s32)r.readu32le();
    t.angle = r.readu32le();
    t.flags = Trigger::Flag(r.readu32le());
  }

  ASSERT(r.m_position == rawSize);
}

void checkTrack(const Track &track) {
  // More user-friendly would be to emit a message for each assert violation
  // pointing to the corresponding trigger instead of bailing on first fail.

  ASSERT(track.trackType < Track::Type::Count);

  ASSERT(track.tickStart == 0);
  ASSERT(track.tickEnd == track.tickCount);

  ASSERT(track.featureZoneStart <= track.featureZoneEnd);
  ASSERT(track.summonStart <= track.summonEnd);
  ASSERT(track.featureZoneEnd <= track.summonStart);

  if (track.isBMS()) {
    // summonTrigger actually has no effect in game
    //
    // ASSERT((track.summonStart <= track.summonTrigger) &&
    //        (track.summonTrigger <= track.summonEnd));
  } else if (track.isFMS()) {
    ASSERT(track.summonTrigger == 0);
  } else if (track.isEMS()) {
    ASSERT(track.summonTrigger == 0);
  }

  u32 prevTick = 0;

  for (u32 i = 0; i < track.triggerCount; ++i) {
    const Trigger &t = track.triggers[i];
    ASSERT(t.type < Trigger::Type::Count);

    ASSERT(track.tickStart < t.tick && t.tick < track.tickEnd);

    if (i > 0) {
      ASSERT(t.tick > prevTick);
    }
    prevTick = t.tick;

    if (track.isBMS()) {
      ASSERT(t.type != Trigger::TrackGuide && t.type != Trigger::Holdlet);
      ASSERT(t.x == 0);
      ASSERT(0 <= t.y && t.y < 4);
      ASSERT(t.flags == Trigger::Flag::None);
    } else if (track.isFMS()) {
      ASSERT(t.type < Trigger::TrackGuide);
      ASSERT(t.x == 0);
      ASSERT(0 <= t.y && t.y <= 100);
      ASSERT(t.flags == Trigger::Flag::None);
    } else if (track.isEMS()) {
      if (t.type == Trigger::TrackGuide) {
        ASSERT(-150 <= t.x && t.x <= 150);
        ASSERT(-75 <= t.y && t.y <= 75);
      } else {
        ASSERT(t.x == 0 && t.y == 0);
        ASSERT(t.flags == Trigger::Flag::None ||
               t.flags == Trigger::Flag::AbsoluteAngle);
      }
    }

    ASSERT(t.angle < 360);
  }
}

static usize getTrackWriteSize(const Track &track) {
  return 10 * sizeof(u32) + track.triggerCount * 6 * sizeof(u32);
}

void writeTrackToBuffer(const Track &track, u8 *raw, u32 rawSize) {
  ENSURE(raw != nullptr);
  ENSURE(rawSize >= getTrackWriteSize(track));

  WriteStream r = newWriteStream(raw);

  u32 tickStart = 0;
  u32 tickEnd = track.tickCount;
  u32 summonTrigger = track.summonEnd;

  r.writeu32le(track.trackType);
  r.writeu32le(track.tickCount);
  r.writeu32le(tickStart);
  r.writeu32le(tickEnd);
  r.writeu32le(track.featureZoneStart);
  r.writeu32le(track.featureZoneEnd);
  r.writeu32le(track.summonStart);
  r.writeu32le(track.summonEnd);
  r.writeu32le(summonTrigger);
  r.writeu32le(track.triggerCount);

  for (u32 i = 0; i < track.triggerCount; ++i) {
    const Trigger &t = track.triggers[i];
    r.writeu32le(t.tick);
    r.writeu32le(t.type);
    r.writeu32le((u32)t.x);
    r.writeu32le((u32)t.y);
    r.writeu32le(t.angle);
    r.writeu32le(t.flags);
  }

  ASSERT(r.m_position == rawSize);
}

void readTrackFromFile(Track *track, const char *filename) {
  usize fileSize = getFileSize(filename);

  u8 *fileBuf = (u8 *)allocMem(fileSize);
  readFile(filename, fileBuf, fileSize);

  u32 uncompressedSize = getLZ11UncompressedSize(fileBuf);
  u8 *uncompressedBuf = (u8 *)allocMem(uncompressedSize);

  decompressLZ11(fileBuf, fileSize, uncompressedBuf, uncompressedSize);

  freeMem(fileBuf);

  readTrackFromBuffer(track, uncompressedBuf, uncompressedSize);

  freeMem(uncompressedBuf);
}

void writeTrackToFile(const Track &track, const char *filename) {
  usize rawSize = getTrackWriteSize(track);
  u8 *raw = (u8 *)allocMem(rawSize);

  writeTrackToBuffer(track, raw, NumCast<u32>(rawSize));

  u8 *compressed = (u8 *)allocMem(rawSize);

  usize compressedSize = compressLZ11(raw, rawSize, compressed, rawSize);
  writeFile(filename, compressed, compressedSize);

  freeMem(raw);
  freeMem(compressed);
}

void printTrackStats(const Track &track) {
  printf("%s\n%d ticks\n%d--%d feature zone\n%d--%d summon\n",
         TRACK_TYPE_NAMES[track.trackType], track.tickCount,
         track.featureZoneStart, track.featureZoneEnd, track.summonStart,
         track.summonEnd);

  u32 triggerTypeCount[Trigger::Type::Count];

  for (u32 i = 0; i < Trigger::Type::Count; ++i)
    triggerTypeCount[i] = 0;

  for (u32 i = 0; i < track.triggerCount; ++i) {
    triggerTypeCount[track.triggers[i].type]++;
  }

  printf("%d triggers\n", track.triggerCount);
  for (u32 i = 0; i < Trigger::Type::Count; ++i) {
    printf("  %3d %s\n", triggerTypeCount[i], TRIGGER_TYPE_NAMES[i]);
  }
}

} // namespace rideau
