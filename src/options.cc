#include "options.h"

#include <unistd.h>

#include "utils.h"

namespace rideau {

int parseArgs(int argc, char *argv[], Options *options) {
  int opt;
  options->batchMode = false;

  while ((opt = getopt(argc, argv, "b")) != -1) {
    switch (opt) {
    case 'b':
      options->batchMode = true;
      break;
    default:
      return 1;
    }
  }

  if (argc - optind < 2) {
    return 1;
  }

  ENSURE((optind + 1) < argc);

  options->triggerFile = argv[optind];
  options->musicFile = argv[optind + 1];

  return 0;
}

} // namespace rideau
